# Telexistence ROS Bridge

Telexistence's ROS-WEB Bridge for now based on the roslibjs, and rosbridge. This package serves the purpose of a bridge between the ROS and Web world. It is intended to provide a REST API in python, but currently in under planning.  

## Dependencies Installation
`$ sudo apt-get install ros-kinetic-rosbridge-suite`

## Running 

* `$ roscore `
* `$ roslaunch rosbridge_server rosbridge_websocket.launch`
* open simple.html on Chrome. 

## Future Features 

* Fully functional REACT web interface. 
* Redis server for storing data coming from ROS and Web Interface. 
* Python Backend to replace roslibjs.

## References

* [ROS Bridge](http://wiki.ros.org/rosbridge_suite/Tutorials/RunningRosbridge)
* [roslibjs](http://wiki.ros.org/roslibjs/Tutorials/BasicRosFunctionality)


# Current Design
![Design](images/design.png)

# API Description: Exposed Topics, Services, Params

TBD